
# PBST Handbook
## All PBST Officers
### (ST. 1) Follow all Roblox rules
All PBST Members must abide by all ROBLOX Community Rules, which can be found [here](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules)

### (ST. 2) Not at war with Innovation
We are not at war with Innovation, so please do not "raid" Innovation Games.

### (ST. 3) Keep the core stable
As an Officer of PBST, you must keep the core stable, meaning you can't melt or freeze the core. If you need help, you can refer to this chart to help you (Credit to TenX29 for the chart.) ![this](https://cdn.discordapp.com/attachments/459764670782504961/536944134796214292/pbcc_stable_combinations_jan-21-2019.png)

### (ST. 4) Do not become a mutant
You, as a PBST Officer, are not permitted to become a mutant. Mutants, including any PBST Officers who are mutants, are always Kill on Sight (KoS).

### (ST. 5) Warn visitors
Officers must give two warnings to a visitor before killing them. After a visitor is killed, they only have to be warned once to be killed again. If a visitor is armed, attacking you or another security officer, or blocking a room, they do not need a warning to be killed.

### (ST. 6) Dont abuse weapons
You may not abuse the weapons you receive, and any PBST Officers that use OP Weapons in their duties must follow PBST Rules instead of OP Rules. 'PBST Weapon Abuse' is classified as attacking without reason, mass random killing (RK), and spawn-killing (SK).

### (ST. 7) Respect others
Always respect your superiors and follow their orders.

### (ST. 8) Help guest's first
Always put guest's lives before yours and always evacuate guests first.

### (ST. 9) Official uniforms
Official uniforms are mandatory at any official Pinewood Builders facility, and custom uniforms are not allowed unless they are made official. Official uniforms can be found [here](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

### (ST. 10) Don't lend weapons
You may not give weapons to people unless authorized by a High Rank.

### (ST. 11) No off-duty work
Going off-duty is permitted, however once Off-Duty, you can't get any PBST related gear or uniforms. If there is a raid that is about to happen, you must choose a team before the raid, and may not go off-duty during said raid. Changing teams midway through the raid is not permitted. PBST Officers may not wear PET Suits while on-duty.

### (ST. 12) No free stuff
Do not ask for free points or ranks.


## Trainee's
### (TE. 1) Open to criticism
Trainees must be open to criticism from other members, when you're a trainee, you will only have acces to the baton and the security room.

## Tiers

### Required Points
Tier 1: 72    
Tier 2: 240    
Tier 3: 360    

### (TR. 1) Be a role model
Tiers are a role model for Trainees, and will receive more severe punishments for breaking a handbook rule than trainees.

### (TR. 2) !report
Tiers have access to the !report command at many PB Facilities. Please use this command appropriately, otherwise there may be punishments.

### (TR. 3) Assist in training
As a Tier, you may be selected to assist at a training. As a result you will be granted temporary admin powers. Abuse of these powers will result in severe punishment.

### (TR. 4) Authorised Personnel Only
Tiers may not let anyone below their rank into the Tier only rooms.



## Special Defence
### Required Points
Required Point Amount: 500

### (SD. 1) With great power comes great responsibility
Special Defense is trusted with moderator powers at PBST training facilities. Abuse these and a severe punishment will be swiftly carried out.

### (SD. 2) No rule (ST. 9)
Special Defense are exempt from rule (ST. 9).

### (SD. 3) Ask Permission!
SD must get permission from a Trainer to host a training, and must notify the same Trainer of any changes they are making to the training.

### (SD. 4) Note the time!
SD must abide by the 1 hour training policy, in which you can't host a training 1 hour after a previous training. Mega trainings don't fall under the 1 hour policy, however please do leave at least 30 minutes before the Mega training so everyone can get there in time.

### (SD. 5) Use discord!
It is highly recommended that Special Defense have Discord.


## Trainers

### (TN. 1) NO RULES (Exept (ST. 1))
Trainers are exempt from all (ST) rules except (ST. 1).

### (TN. 2) Note the time!
Trainers must also abide by the 1 hour training policy.

### (TN. 3) Override SD's
Trainers can override anything done by SD.

### (TN. 4) Must have discord!!!
Trainers are required to have Discord.
