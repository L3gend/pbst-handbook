﻿---
home: true
heroImage: /PBST-Logo.png
actionText: Los geht's →
actionLink: pbst/
---
::: warning ANMERKUNG:
Das Handbuch wurde ursprünglich von RogueVader1996 erstellt.
:::
::: danger ACHTUNG!
Bitte lest alle Regeln aufmerksam durch. Jegliche Änderung an den Regeln wird klargestellt. Die Regeln sind kategorisiert nach der Gruppe auf die sie zutreffen, wobei einige Regeln auf mehrere Bereiche zutreffen, in diesem Fall wird dies deutlich gemacht. Regelbrüche werden je nach Regel und Schwere des Regelbruchs bestraft. Bestrafungen reichen von simplen Warnungen, Degradierungen bis zu einem Ausschluss aus PBST.
:::