
# Welcome to the handbook!
This is the index page. Here you will be able to select any page

* [PBST Handbook](handbook/)
  * [Uniform](handbook/#uniform)
  * [On-duty](handbook/#on-duty)
  * [Pinewood Computer Core (PBCC)](handbook/#pinewood-computer-core-pbcc)
  * [Usage of weapons](handbook/#usage-of-weapons)
  * [The Mayhem Syndicate](handbook/#the-mayhem-syndicate)
  * [Kill on Sight (KoS)](handbook/#kill-on-sight-kos)
  * [Trainings and ranking up](handbook/#trainings-and-ranking-up)
  * [Tiers](handbook/#tiers)
  * [Tier 4 (aka Special Defense, SD)](handbook/#tier-4-aka-special-defense-sd)
  * [Trainers](handbook/#trainers)
* [All the facilities](all-the-facilities/)
  * [Pinewood Computer Core (PBCC)](all-the-facilities/#pinewood-computer-core-pbcc)
  * [Pinewood Space Shuttle Advantage (PBSSA)](all-the-facilities/#pinewood-space-shuttle-advantage-pbssa)
  * [Pinewood Research Facility (PBRF)](all-the-facilities/#pinewood-research-facility-pbrf)
  * [Pinewood Headquarters (PBHQ)](all-the-facilities/#pinewood-headquarters-pbhq)
  * [Pinewood Builders Data Storage Facility (PBDSF)](all-the-facilities/#pinewood-builders-data-storage-facility-pbdsf)
  * [PBST Training Facility (PBSTTF)](all-the-facilities/#pbst-training-facility-pbsttf)
  * [PBST Activity Center (PBSTAC)](all-the-facilities/#pbst-activity-center-pbstac)
  * [PBST Hub (PBSTH)](all-the-facilities/#pbst-hub-pbsth)


