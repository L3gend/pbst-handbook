
# PBST Handbook
## Uniform
The official PBST uniforms can be found in the store [here](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

If you have no robux, most facilities are equipped with uniform givers to get you going. It’s still recommended that you buy a uniform for your convenience.

You must wear an official PBST uniform when patrolling a Pinewood facility. If you are in an official PBST regiment, you can wear their uniform to patrol in accordance with their rules.

Wearing a PET uniform is not allowed, the only exception is at PBCC when the core temperature makes it impossible to wear a standard core suit and a PET Hazmat suit is the only way to enter the core (3000C+). Once the core temp allows for a standard core suit again (3000C-), you must switch back to said suit.

## On-duty
You need to be wearing an official PBST uniform to be considered on-duty. Your ranktag must also be on and set to Security (which is the default), though having just the ranktag does not make you on duty.

As a PBST Officer on duty, you are to protect the facility from any threat that arises. When you have to evacuate, try your hardest to help the others.

To go off-duty, you have to visit the Security room and remove your uniform and PBST weapons. Reset character if required. It is highly recommended you remove your ranktag as well, by using the command ``!ranktag off``.

## Pinewood Computer Core (PBCC)
The [Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) is the most important place to patrol. Your main objective here is to prevent the core from melting or freezing. The following chart shows you how different settings affect the temperature. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228"/>

PBCC also features various ‘disasters’, some of which require PBST to evacuate the visitors. In the event of a plasma surge, earthquake or radiation flood, bring the people in danger to safety.

## Usage of weapons
Every PBST member has access to the standard baton, and Tiers receive additional weapons from the blue loadouts. Be sure to have your uniform on when taking these weapons, for you are considered off-duty without uniform and you may not use PBST Weapons off-duty.

You have to give **two** warnings to people before you can kill them. If a visitor has been killed after three warnings and still returns, you can kill that person without warning. On-duty PBST members also have to follow these rules when using non-PBST weapons like the OP Weapons gamepass, or the pistol acquired from PBCC Credits or randomly spawned.

When off-duty, you may only use non-PBST Weapons, though you have more freedom with your usage. **PBST weapons may never be used to cause a melt-or freezedown.**

Mass random killing and spawnkilling are always forbidden, use the ``!call`` command to call PIA to your assistance if someone is mass random killing or spawnkilling.

## The Mayhem Syndicate
The Mayhem Syndicate (**TMS**) is the opposite of PBST. Where we intend to protect the core, they intend to melt or freeze it. You can recognize them by the red ranktag. They often join a game in a raid, and when this happens you are advised to call for backup.

The only place where TMS may not be killed is at the TMS loadouts near the cargo trains. You have to give TMS a fair chance to take their loadouts, as they will do the same for PBST loadouts. Do not camp at the TMS loadouts.

If you are off-duty when TMS starts to raid and you want to participate, you have to pick a side and fight on that side until the raid ends. **Do not change sides mid-raid.** This also applies if you are neutral or off-duty and decide to participate anyway. Beware that choosing to side for either TMS or PBST in a raid might get you put on KoS by the other side.

## Kill on Sight (KoS)
Any member of TMS is to be killed on sight.

Mutants are KoS as well. **PBST members may not become mutants while on duty.**

On-duty PBST may not put anyone else on KoS unless permission has been granted by a Tier 4+.

## Trainings and ranking up
::: warning NOTE:
Instructions in trainings are given in English, so understanding English is required.
:::
Ranking up requires points, which can be earned in trainings and patrols. If you get 100 points you are promoted to Tier 1, which gives you a more powerful baton, a riot shield, a taser, and a PBST pistol. With 250 points you become Tier 2, and you receive all the former plus a rifle. At 500 points you become Tier 3 where you receive all the former weapons plus a submachine gun. These weapons can be found in the Security room at Pinewood facilities, and Cadets are not allowed in there.

Tier 4+ can host trainings where you can earn points. The schedule of these trainings can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Always wear a PBST uniform in these trainings, no regiment uniforms. Listen to the host and follow his/her orders.

The better you perform, the more points you get. The maximum amount of points you can earn in a normal training is 5. Every Saturday & Sunday at 5:00 PM UTC there is a Mega Training, where double points can be earned.

You can also train yourself at [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center), Sector 2. These points are purely skill-based.

Be also on the lookout for TMS, their raids often allow you to earn points as well.

## Tiers
Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the !call PBST command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at a training, where you will be given temporary admin powers which are **strictly** for that specific training. Abuse of these powers will result in severe punishment.

## Tier 4 (aka Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

As Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host trainings with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request a training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Reminders to all PBST members
  * Please abide by the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) at all times.
  * Be respectful to your fellow players, don’t act like you’re the boss of the game.
  * Follow the orders of anyone who outranks you.

::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::
