﻿---
home: true
heroImage: /PBST-Logo.png
actionText: 觀看手冊 →
actionLink: pbst/
---
::: warning 請注意:
這本手冊是由RogueVader1996所創作的
:::
::: danger 警告
請仔細閱讀所有規則，所有規則的更改將會明確列出。
這些規則已被歸類為適用的身份類別，但是某些規則可能適用於不止一個階級的PBST成員而在這種情況下將會清楚列出。 
違反規則的處分取決於違反什麼規則和事情的嚴重性。
處分可以從最簡單的警告，降級，到最嚴重被列入PBST的封禁名單
:::