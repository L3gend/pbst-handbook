---
sidebar: auto
---
# Credits where there due
We would like to following people for helping us with the translations to their own languages.

## Backend + Development
* Stefano / superstefano4

## Translations
### Chinese
* aa898246[中華民國]
* CuberPro47
* cccccccccc

### German
* PilleniusMC

### Finnish
* TenX 

## Proofreading

### PIA
* Omni
### Trainers
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
