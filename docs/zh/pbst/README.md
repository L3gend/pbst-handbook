﻿
# 歡迎查看這本手冊!
這里是目錄頁，你可以在這裡選擇其他的頁面

* [PBST手冊](handbook/)
  * [制服](handbook/#uniform)
  * [執勤](handbook/#on-duty)
  * [Pinewood Builders 電腦核心(PBCC)](handbook/#pinewood-computer-core-pbcc)
  * [槍械的使用](handbook/#usage-of-weapons)
  * [The Mayhem Syndicate](handbook/#the-mayhem-syndicate)
  * [Kill on Sight (KoS)](handbook/#kill-on-sight-kos)
  * [訓練及升階](handbook/#trainings-and-ranking-up)
  * [階級](handbook/#tiers)
  * [第四階(特別防守組)](handbook/#tier-4-aka-special-defense-sd)
  * [訓練員](handbook/#trainers)
* [所有設施](all-the-facilities/)
  * [Pinewood Computer Core (PBCC)](all-the-facilities/#pinewood-computer-core-pbcc)
  * [Pinewood Space Shuttle Advantage (PBSSA)](all-the-facilities/#pinewood-space-shuttle-advantage-pbssa)
  * [Pinewood Research Facility (PBRF)](all-the-facilities/#pinewood-research-facility-pbrf)
  * [Pinewood Headquarters (PBHQ)](all-the-facilities/#pinewood-headquarters-pbhq)
  * [Pinewood Builders Data Storage Facility (PBDSF)](all-the-facilities/#pinewood-builders-data-storage-facility-pbdsf)
  * [PBST Training Facility (PBSTTF)](all-the-facilities/#pbst-training-facility-pbsttf)
  * [PBST Activity Center (PBSTAC)](all-the-facilities/#pbst-activity-center-pbstac)
  * [PBST Hub (PBSTH)](all-the-facilities/#pbst-hub-pbsth)


