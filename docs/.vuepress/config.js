module.exports = {
  dest: 'public/',
  locales: {
    '/': {
      lang: 'en-US',
      title: 'PBST Handbook',
      description: 'The unofficial PBST Handbook'
    },
    '/nl/': {
      lang: 'nl-NL',
      title: 'PBST Handboek',
      description: 'Het onofficiele PBST Handboek'
    },
    '/fi/': {
      lang: 'fi-FI',
      title: 'PBST-käsikirja',
      description: 'Epävirallinen PBST-käsikirja'
    },
    '/zh/': {
      lang: 'zh-tw',
      title: 'PBST手冊',
      description: '非官方手冊'
    },
    '/de/': {
      lang: 'de-DE',
      title: 'PBST Handbuch',
      description: 'Das inoffizielle PBST Handbuch'
    }
  },
  themeConfig: {
    repo: 'https://gitlab.com/TCOOfficiall/PBST-Handbook',
    editLinks: true,
    docsDir: 'docs/',
    logo: '/PBST-Logo.png',
    smoothScroll: true,
    locales: {
      '/nl/': {
        label: 'Dutch',
        selectText: 'Talen',
        ariaLabel: 'Selecteer taal',
        editLinkText: 'Verander deze pagina op GitLab',
        lastUpdated: 'Laast geupdate',
        nav: require('./nav/nl'),
        sidebar: {
          '/nl/pbst/': [
            'handbook/',
            'all-the-facilities/'
          ]
        }
      },
      '/fi/': {
        label: 'Finnish',
        selectText: 'Kielet',
        ariaLabel: 'Valitse kieli',
        editLinkText: 'Muokkaa tätä sivua GitLabissa',
        lastUpdated: 'Viimeksi päivitetty',
        nav: require('./nav/fi'),
        sidebar: {
          '/fi/pbst/': [
            'handbook/',
            'all-the-facilities/'
          ]
        }
      },
      '/zh/': {
        label: 'Chinese',
        selectText: '語言',
        ariaLabel: '選擇語言',
        editLinkText: '在GitLab上編輯此頁面',
        lastUpdated: '最近更新',
        nav: require('./nav/zh'),
        sidebar: {
          '/zh/pbst/': [
            'handbook/',
            'all-the-facilities/'
          ]
        }
      },
      '/de/': {
        label: 'German',
        selectText: 'Sprachen',
        ariaLabel: 'Sprache auswählen',
        editLinkText: 'Diese Seite Auf GitLab bearbeiten',
        lastUpdated: 'Letzte Änderung',
        nav: require('./nav/de'),
        sidebar: {
          '/de/pbst/': [
            'handbook/',
            'all-the-facilities/'
          ]
        }
      },
      '/': {
        label: 'English',
        selectText: 'Languages',
        ariaLabel: 'Select language',
        editLinkText: 'Edit this page on GitLab',
        lastUpdated: 'Last Updated',
        nav: require('./nav/en'),
        sidebar: {
          '/pbst/': [
            'handbook/',
            'all-the-facilities/'
          ]
        }
      },
    }
    
  },
  extraWatchFiles: [
    '/nav/en.js',
    '/nav/nl.js',
    '/nav/fi.js',
    '/nav/de.js'
  ],
  sidebarDepth: 2,
  plugins: [
    [
      'vuepress-plugin-zooming',
      {
        selector: '.theme-default-content img.zooming',
        delay: 1000,
        options: {
          bgColor: 'white',
          zIndex: 10000,
        },
      },
    ],
  ],
}