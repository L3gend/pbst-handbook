# PBST-käsikirja
## Univormu
Viralliset univormut löytyvät [täältä](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Mikäli sinulla ei ole robuxeja, univormut ovat saatavilla suurimmassa osassa laitoksista. Univormun ostaminen on kuitenkin suositeltavaa kätevyyden vuoksi.

Sinun täytyy pitää ylläsi virallista PBST-univormua partioidessasi Pinewood-laitoksia. Jos kuulut virallisen PBST-rykmenttiin/divisioonaan, voit käyttää kyseisen ryhmän univormua partioidessasi ryhmän sääntöjen mukaisesti.

PET-univormun käyttäminen ei ole sallittua, ainoana poikkeuksena on PBCC:ssä, kun reaktoriytimen lämpötila estää ytimeen menemisen tavallisessa säteilypuvussa ja PET Hazmat -puku on ainoa tapa päästä reaktoriytimeen (> 3000°C). Kun lämpötila sallii taas tavallisen säteilypuvun käytön (< 3000°C), täytyy sinun vaihtaa takaisin kyseiseen pukuun tai PBST-univormuun.

## Palveluksessa
::: warning HUOMIO:
Tässä osiossa ja seuraavissa käsikirjan osioissa "palveluksessa oleminen" viittaa laitoksen partiointiin alla olevien ohjeiden mukaisesti.
:::

Sinulla täytyy olla ylläsi virallinen PBST-univormu, jotta sinut lasketaan olevan palveluksessa. Arvomerkkisi (ranktag) täytyy myös olla päällä asetuksella "Security" (joka on oletusasetus). Pelkkä päällä oleva arvomerkki ei riitä pitämään sinua palveluksessa olevana.

Palveluksessa olevana PBST-jäsenenä sinun tulee suojella laitosta kaikilta ilmaantuvilta uhilta. Kun sinun täytyy evakuoida, yritä parhaasi mukaan auttaa muita.

Poistuaksesi palveluksesta tai mennäksesi vapaalle, sinun täytyy mennä vartijaosastolle poistamaan itseltäsi kaikki PBST-aseet ja univormu. Resetoiminen/respawnaaminen on toinen vaihtoehto. Arvomerkin poistaminen on myös suositeltavaa komennolla ``!ranktag off``.

## Pinewood Computer Core (PBCC)
[Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) on kaikkein tärkein partioitava laitos. Päätavoitteesi täällä on estää reaktoriytimen sulaminen tai jäätyminen. Seuraava kaavio näyttää, miten eri asetukset vaikuttavat ytimen lämpötilaan. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228"/>

PBCC:ssä on myös erilaisia ‘katastrofeja’, joista osa vaatii PBST:tä evakuoimaan kävijöitä. Plasmapurkauksen, maanjäristyksen tai säteilyturvan sattuessa saata vaarassa olevat henkilöt turvaan.

## Aseiden käyttö
Jokaisella PBST-jäsenellä on käytettävissään tavallinen pamppu, ja Tier 1+ -jäsenet saavat käyttöönsä ylimääräisiä aseita sinisistä välineistöistä. Muista pitää univormuasi päällä ottaessasi näitä aseita, sillä sinun katsotaan olevan vapaalla ilman univormua ja arvomerkkiä, ja PBST-aseiden käyttö on tällöin kielletty.

Sinun täytyy antaa **kaksi** varoitusta muille ennen tappavan voiman käyttöä. Jos kävijä on jo kerran tapettu kolmen varoituksen jälkeen, voit käyttää tappavaa voimaa varoituksetta. Palveluksessa olevien PBST-jäsenten tulee noudattaa näitä sääntöjä myös käyttäessään muita kuin PBST-aseita.

Vapaalla ollessasi saat käyttää vain muita kuin PBST-aseita, mutta sinulla on enemmän vapauksia niiden käytön suhteen. **PBST-aseita ei saa koskaan käyttää reaktoriytimen jäätymisen tai sulamisen aiheuttamiseen.**

Muiden pelaajien satunnaisesti tappaminen suurissa määrin ja spawnin läheisyydessä tappaminen on aina kielletty. Käytä ``!call``-komentoa kutsuaksesi PIA-agentti apuusi, jos joku rikkoo tätä sääntöä.

## The Mayhem Syndicate
The Mayhem Syndicate (**TMS**) on PBST:n vastakohta. Siinä missä me yritämme suojella reaktoriydintä, he pyrkivät sulattamaan tai jäädyttämään sen. Voit tunnistaa nämä pelaajat punaisesta arvomerkistä. He liittyvät usein peliin hyökkäyksen aikana. Tämän tapahtuessa sinua kehotetaan kutsumaan apujoukkoja.

Spawnin lisäksi TMS-jäseniä ei saa tappaa TMS-välinevarastolla rahtijunien läheisyydessä. Sinun tulee antaa TMS-jäsenille reilu mahdollisuus välineiden noutamiseen, sillä he tekevät saman PBST-jäsenille. Älä oleskele TMS-välineistön luona.

Jos olet vapaalla TMS:n aloittaessa hyökkäyksen ja haluat osallistua, sinun täytyy valita puoli ja pysyä kyseisellä puolella tapahtuman loppuun asti. **Älä vaihda puolta hyökkäyksen aikana.** Tämä pätee myös, vaikka olisit neutraali ja/tai vapaalla ja haluat osallistua joka tapauksessa. Huomaa, että valitessasi puoleksesi PBST:n tai TMS:n, vastustajapuoli voi asettaa sinut KoS:lle.

## Kill on Sight (KoS, tapa suoralta kädeltä)
Jokainen TMS-jäsen, jolla on punainen ranktag, tulee tappaa heti.

Mutantit tulee myös tappaa välittömästi. **PBST-jäsenet eivät saa muuttua mutanteiksi palveluksessa ollessaan.**

Palveluksessa olevat PBST-jäsenet eivät saa antaa KoS-käskyjä, paitsi jos Tier 4+ -jäsen on antanut siihen luvan.

## Koulutukset ja ylennysten saaminen
::: warning HUOMIO:
Koulutuksissa ohjeet annetaan englanniksi, joten englannin kielen ymmärrystä vaaditaan.
:::

Ylennyksen saaminen vaatii pisteitä, joita voi ansaita koulutuksissa ja järjestetyissä partioinneissa. Jos saat 100 pistettä, sinut ylennetään Tier 1 -arvoon, joka antaa sinulle voimakkaamman pampun, mellakkakilven, etälamauttimen ja PBST-pistoolin. 250 pisteellä sinusta tulee Tier 2 -arvoinen, ja saat kaiken edellä mainitun lisäksi kiväärin. 500 pisteellä saavutat Tier 3 -arvon, millä edellisten aseiden lisäksi saat käyttöösi myös konepistoolin. Nämä aseet löytyvät vartijaosastoilla sijaitsevista huoneista Pinewood-laitoksissa, eikä kadetteja sallita näihin huoneisiin.

Tier 4+ -jäsenet voivat järjestää koulutuksia, joissa voit ansaita pisteitä. Näiden koulutusten aikataulun voi lukea [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility)ssä. Pidä ylläsi aina virallista PBST-univormua näissä koulutuksissa, älä regimenttien/divisioonien univormuja. Kuuntele ohjaajaa ja noudata hänen antamia käskyjä.

Mitä paremmin suoriudut, sitä enemmän pisteitä saat. Normaalissa koulutuksessa voit ansaita enintään 5 pistettä. Joka lauantai ja sunnuntai kello 5:00 PM UTC pidetään suurempi koulutus (Mega Training, MT), jossa voi ansaita kaksinkertaisen määrän pisteitä.

Voit myös kouluttaa itseäsi [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center)issä, Sektorilla 2. Tästä ansaitut pisteet ovat puhtaasti taitoon perustuvia.

Ole myös valmiina mahdollisiin TMS-hyökkäyksiin, sillä myös näistä voi usein ansaita pisteitä.

## Tier-jäsenet
Kun sinut ylennetään Tier-jäseneksi (Tier 1 -arvo tai ylempi), sinun oletetaan toimivan esikuvana kadeteille. Sääntörikkomukset saattavat johtaa suurempiin rangaistuksiin. Erityisesti Tier 3 -jäsenten odotetaan olevan PBST:n parhaimpia edustajia. **Sinua on varoitettu.**

Tier-jäsenet voivat käyttää ``!call PBST`` -komentoa, jota he voivat käyttää reaktoriytimen sulamisen tai jäätymisen uhatessa tai hyökkääjän kohdatessaan. Tätä komentoa tulee käyttää viisaasti.

Tier-jäsenenä sinut saatetaan valita avustamaan koulutuksessa, jolloin sinulle annetaan väliaikaisesti oikeus käyttää ylläpitokomentoja **ainoastaan** kyseistä koulutusta varten. Näiden komentojen väärinkäyttö johtaa ankaraan rangaistukseen.

## Tier 4 (tunnetaan myös nimillä Special Defense, SD)
Tier 3 -jäsenet, joilla on vähintään 800 pistettä, ovat kelvollisia SD-arviointiin tullakseen Tier 4 -jäseniksi. Mikäli sinut valitaan, saat tehtäväksesi järjestää PBST-koulutuksen ja suoritustasi valvotaan tarkasti. Jos pääset läpi, sinulle annetaan “Passed SD Eval” (Läpäissyt SD-arvioinnin) -arvonimi, kunnes kouluttajat (Trainer) päättävät ylentää sinut Tier 4 -arvoon. Discord-käyttäjätunnus vaaditaan.

Tier 4 -jäsenenä saat oikeuden antaa KoS-käskyjä Pinewood-laitoksissa, ja univormun käyttäminen ei ole sinulle enää pakollista. Saat myös käyttöösi Kronos-moderaattorikomennot PBST-koulutuslaitoksissa. Näitä on käytettävä vastuullisesti.

Tier 4 -jäsenet voivat järjestää koulutuksia kouluttajan (Trainer) luvalla. He saavat järjestää enintään 6 koulutusta viikossa (Mega Training -koulutukset pois lukien) ja enintään 2 koulutusta päivässä. Tier 4 -jäsenet eivät saa pyytää lupaa koulutuksen järjestämiseen yli 3 päivää etuajassa. Koulutusten välissä täytyy myös olla vähintään 2 tunnin väli edellisen koulutuksen loppumisesta seuraavan alkuun.

## Kouluttajat (tunnetaan myös nimellä Trainer)
Kouluttajaksi päästäksesi kaikkien nykyisten kouluttajien täytyy äänestää ylennyksestäsi. Vain Tier 4 -jäsenet ovat kelpuutettuja tähän ylennykseen.

Kouluttajat voivat järjestää koulutuksia rajoituksetta, poikkeuksena aina voimassa pysyvä kahden tunnin välin sääntö. Kouluttajat ovat myös vastuussa PBST:n hallintotehtävistä, kuten pisteiden kirjaamisesta ja ylennyksistä. Älä kehota kouluttajaa kirjaamaan pisteitä tai ylentämään sinua, vaan ole kärsivällinen.

Laitoksia partioivilla kouluttajilla on vapaus poiketa suurimmasta osasta käsikirjan säännöistä, mutta seuraavat muistutukset koskevat myös heitä.

# Muistutuksia kaikille PBST-jäsenille
  * Noudata aina [ROBLOXin yhteisösääntöjä](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Kunnioita muita pelaajia, äläkä käyttäydy, niin kuin olisit pelin johtaja.
  * Noudata korkeampiarvoisten jäsenten antamia käskyjä.

::: warning HUOMIO:
Näitä sääntöjä voidaan muuttaa milloin tahansa. Tarkista käsikirja silloin tällöin mahdollisten päivitysten varalta.
:::
