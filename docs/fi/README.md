﻿---
home: true
heroImage: /PBST-Logo.png
actionText: Käsikirjaan →
actionLink: pbst/
---
::: warning HUOMIO:
Käsikirjan on alun perin tehnyt RogueVader1996. Kaikki kunnia noudattamiemme sääntöjen tekemisestä kuuluu hänelle.
:::
::: danger VAROITUS!
Lue kaikki säännöt erittäin huolellisesti. Kaikista sääntöjen muutoksista tiedotetaan selvästi. Säännöt on luokiteltu sen mukaan, ketä ne koskettavat, mutta osa säännöistä saattaa koskea useampaa kuin yhtä PBST-jäsenten ryhmää. Tässä tapauksessa asiasta kerrotaan selvästi. Säännön rikkomisesta seuraavat rangaistukset riippuvat säännöstä ja rikkomuksen vakavuudesta. Rangaistukset voivat vaihdella varoituksen, arvonalennuksen tai jopa mustalle listalle asettamisen välillä.
:::