﻿---
home: true
heroImage: /PBST-Logo.png
actionText: Let's do this →
actionLink: pbst/
---
::: warning NOTE:
Het handboek is oorspronkelijk gemaakt door RogueVader1996. Alle eer voor het maken van de regels waar we op staan, is voor hem.
:::
::: danger WARNING!
Lees alle regels aandachtig door. Eventuele wijzigingen in de regels worden duidelijk gemaakt. De regels zijn onderverdeeld in op wie ze van toepassing zijn, maar sommige regels kunnen op meer dan één groep PBST-leden van toepassing zijn, in welk geval dit duidelijk zal worden gemaakt. Straffen voor het overtreden van een regel hangen af van welke regel en de ernst. Straffen kunnen variëren van een eenvoudige waarschuwing, degradatie of zelfs een zwarte lijst van PBST.
:::