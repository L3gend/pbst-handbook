# All the facility's
## Pinewood Computer Core (PBCC)
Verken een geheime ondergrondse faciliteit die op ontploffen staat! Jouw missie is om te voorkomen dat een supercomputer oververhit raakt. Verken elke hoek van de faciliteit, overleef rampen, druk op elke knop, ontdek geheimen en je komt er misschien levend uit!

📙 Deze game was te zien in het boek Roblox Top Adventure Games, dus probeer het!

- Klik op het onderstaande pictogram om lid te worden van Pinewood Builders voor extra voordelen!

- Helpers + Credits: vooral Irreflexive, Lenemar en Csdi. SigmaTech, Terrariabat, TheGreatOmni, Unsayableorc voor hulp bij wapens en items. Bedankt Jay Cosmic voor toestemming om je lied te gebruiken.

## Pinewood Space Shuttle Advantage (PBSSA)
Lanceer het Space Shuttle-voordeel en rijd de ruimte in! Voltooi dan je satellietimplementatiemissie in een baan en keer terug naar de aarde met je team, bereik het ruimtestation, de maan en andere planeten en raket dan moedig de buitenaardse thuiswereld in om buitenaardse wapens te ontdekken!

Project Advantage, onze inspanningen om een netwerk van onderling verbonden lasersatellieten te lanceren! Deze game was te zien in de Roblox Spotlight-blog en de London Bloxcon 2013. Uitgegeven op 28/28/2012

- Helpers + Credits: RedDwarfIV, Irreflexive, Lenemar, Csdi voor hulp bij wapens

## Pinewood Research Facility (PBRF)
Welkom bij Pinewood Research Facility, gelegen in een geheim deel van de Sahara-woestijn en boven de ondergrondse Pinewood Computer Core. Het dient als de hub tussen onze belangrijkste faciliteiten en games die deel uitmaken van het Sahara-complex - ultramoderne megastructuren en shuttle-lanceringslocaties , evenals de thuisbasis van onze supercomputer mainframe en satellietcommunicatie voor onze ruimtevaartprojecten.

U kunt de tentoonstelling komen bezoeken, waar we u onze technologie en creaties uit het verleden laten zien. Deze plaats beschikt ook over de Pinewood astronaut training centrifuge geschikt voor ongelooflijk hoge G's, dus maak je vast.

Deze faciliteit werd voor het eerst voorgesteld in 2010, na de buitenbedrijfstelling van de oorspronkelijke Pinewood Labs 2008, de onderzoeksfaciliteit voor 2009 en de faciliteiten op het hoofdkantoor van 2009. Alle nog in aanbouw zijnde gebieden worden geblokkeerd met een witte muur.

## Pinewood Headquarters (PBHQ)
Welkom bij ons hoofdkantoor. Voor het eerst gebouwd in 2011, was dit de belangrijkste ontmoetingsplaats en het hoofdkantoor voor Pinewood Builders, gevestigd in Tokio, Japan; we verwelkomen alle bezoekers.

Deze game was te zien in de Roblox Blog 2013, in "[Six ROBLOX Places That'll Make You Say "Whoa"](https://blog.roblox.com/2013/06/six-roblox-places-thatll-make-you-say-whoa/)".

## Pinewood Builders Data Storage Facility (PBDSF)
Dit speciale datacenter in Wyoming slaat punten en andere informatie op voor afdelingen van Pinewood Builders.

Irreflexive - Database Scripter
Csdi - Hoofdbouwer / UI-ontwerp
spyagent388 - Assistent-bouwer
SADENNING - Minor Building, Ideeën

## PBST Training Facility (PBSTTF)
Project CRATER 2015, ook wel bekend als de officiële PBST-trainingsfaciliteit, bevindt zich net buiten Pinewood Research Facility in de grote Sahara. De impactkrater speelt gastheer voor een breed scala aan trainingsactiviteiten en scenario's, evenals gladiatorarena's om de geschoolde te scheiden.

Het klimaat wordt gehandhaafd dankzij een enorme aircokoepel met airconditioning boven de krater en de toegang is via een ultramodern monorailsysteem.
De PBST Shooting dome bevindt zich hier ook. 

## PBST Activity Center (PBSTAC)
Gloednieuwe training, zelftraining en patrouillezone! Deze faciliteit bevat vijf keer de activiteiten die werden gebruikt in het voormalige "Project CRATER" en vele activiteiten in de simulatieruimte. Word lid van PBST om toegang te krijgen tot de faciliteit en geweldige trainingen bij te wonen!

Officieel vrijgegeven voor het publiek vanaf 12 juni 2018.

## PBST Hub (PBSTH)
Deze hub dient als teleportatieplaats voor alle PBST-games, patrouillefaciliteiten en andere games! Het dient ook als een centrale informatieplaats voor nieuwe leden om meer te weten te komen over PBST!
