
# PBST Handbook
## Uniform
De officiële PBST-uniformen zijn te vinden in de winkel [hier](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store)

Als je geen robux hebt, zijn de meeste faciliteiten uitgerust met uniforme gevers om je op weg te helpen. Het wordt nog steeds aanbevolen om voor uw gemak een uniform te kopen.

U moet een officieel PBST-uniform dragen wanneer u patrouilleert in een Pinewood-faciliteit. Als u in een officieel PBST-regiment bent, kunt u hun uniform voor patrouilleren dragen in overeenstemming met hun regels.

Het dragen van een PET-uniform is niet toegestaan, de enige uitzondering is bij PBCC wanneer de kerntemperatuur het onmogelijk maakt om een standaard kernpak te dragen en een PET Hazmat-pak de enige manier is om de kern binnen te komen (3000C +). Zodra de kerntemperatuur weer een standaard kernkleur toestaat (3000C-), moet u teruggaan naar die reeks.

## On-duty
Je moet een officieel PBST-uniform dragen om van dienst te zijn. Je ranktag moet ook zijn ingeschakeld en zijn ingesteld op Beveiliging (de standaardinstelling), maar als je alleen de ranktag hebt, ben je niet verplicht.

Als dienstdoende PBST-officier moet u de faciliteit beschermen tegen elke dreiging die zich voordoet. Wanneer je moet evacueren, doe je best om de anderen te helpen.

Om buiten dienst te gaan, moet je de Beveiligingskamer bezoeken en je uniform en PBST-wapens verwijderen. Reset karakter indien nodig. Het wordt ten zeerste aanbevolen om ook uw ranktag te verwijderen met behulp van het commando ``!ranktag off''.

## Pinewood Computer Core (PBCC)
De [Pinewood Computer Core](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) is de belangrijkste plaats om te patrouilleren. Uw hoofddoel hier is om te voorkomen dat de kern smelt of bevriest. De volgende grafiek toont u hoe verschillende instellingen de temperatuur beïnvloeden. <img class="no-medium-zoom zooming" src="https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png" alt="demo" width="1228"/>

PBCC heeft ook verschillende ‘rampen’, waarvan sommige PBST vereisen om de bezoekers te evacueren. Breng de mensen in gevaar in geval van een stroomstoot in het plasma, aardbevingen of straling.

## Usage of weapons
Elk PBST-lid heeft toegang tot het standaardknuppel en Tiers ontvangen extra wapens van de blauwe uitrusting. Zorg ervoor dat je je uniform aan hebt wanneer je deze wapens neemt, want je wordt beschouwd als buiten dienst zonder uniform en je mag PBST Wapens niet buiten dienst gebruiken.

Je moet **twee** waarschuwingen aan mensen geven voordat je ze kunt doden. Als een bezoeker na drie waarschuwingen is gedood en nog steeds terugkeert, kunt u die persoon zonder waarschuwing doden. Dienstdoende PBST-leden moeten deze regels ook volgen bij het gebruik van niet-PBST-wapens zoals de OP Weapons-gamepass, of het pistool verkregen van PBCC Credits of willekeurig uitgezet.

Buiten dienst mag je alleen niet-PBST-wapens gebruiken, hoewel je meer vrijheid hebt met je gebruik. **PBST-wapens mogen nooit worden gebruikt om een smelt- of bevriezing te veroorzaken.**

Massaal willekeurig doden en spawnkilling zijn altijd verboden, gebruik de opdracht ``!call`` om PIA te helpen als iemand massaal willekeurig moordt of spawnkilled.

## The Mayhem Syndicate
Het Mayhem-syndicaat (**TMS**) is het tegenovergestelde van PBST. Waar we de kern willen beschermen, willen ze deze laten smelten of bevriezen. Je herkent ze aan de rode ranktag. Ze nemen vaak deel aan een spel tijdens een raid, en wanneer dit gebeurt, wordt u geadviseerd om back-up te bellen.

De enige plaats waar TMS niet mag worden gedood, is bij de TMS-loadouts nabij de goederentreinen. Je moet TMS een eerlijke kans geven om hun loadouts te nemen, omdat ze hetzelfde doen voor PBST-loadouts. Kampeer niet bij de TMS-uitrustingen.

Als je buiten dienst bent wanneer TMS begint te plunderen en je wilt meedoen, moet je een partij kiezen en aan die kant vechten tot de aanval eindigt. **Wissel tijdens de raid niet van kant.** Dit geldt ook als je neutraal of buiten dienst bent en toch besluit om deel te nemen. Houd er rekening mee dat als je kiest voor de kant voor TMS of PBST in een aanval, je misschien op KoS aan de andere kant komt.

## Kill on Sight (KoS)
Elk lid van TMS moet op zicht worden gedood.

Mutanten zijn ook KoS. **PBST-leden mogen geen mutanten worden tijdens hun dienst.**

On-duty PBST mag niemand anders op KoS plaatsen tenzij toestemming is verleend door een Tier 4+.

## Trainings and ranking up
::: warning NOTE:
Instrucies en trainingen worden in het Engels uitgevoerdm het verstaan hiervan is heel belangrijk
:::
Voor een hogere rangschikking zijn punten nodig, die kunnen worden verdiend in trainingen en patrouilles. Als je 100 punten krijgt, word je gepromoveerd tot Tier 1, wat je een krachtiger stokje, een oproerhoedje, een taser en een PBST-pistool geeft. Met 250 punten wordt u Tier 2 en ontvangt u alle eerdere plus een geweer. Op 500 punten wordt u Tier 3 waar u alle voormalige wapens plus een machinepistool ontvangt. Deze wapens zijn te vinden in de Beveiligingskamer in Pinewood-faciliteiten, en cadetten zijn daar niet toegestaan.

Tier 4+ kan trainingen organiseren waar u punten kunt verdienen. Het schema van deze trainingen is te vinden op de [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Draag altijd een PBST-uniform in deze trainingen, geen regiment-uniformen. Luister naar de gastheer en volg zijn / haar bevelen.

Hoe beter je presteert, hoe meer punten je krijgt. Het maximale aantal punten dat u kunt verdienen in een normale training is 5. Elke zaterdag en zondag om 17:00 uur UTC is er een Mega-training, waar dubbele punten kunnen worden verdiend.

Je kunt ook jezelf trainen op [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center), sector 2. Deze punten zijn puur op vaardigheden gebaseerd.

Let ook op TMS, hun invallen stellen je vaak in staat om ook punten te verdienen.

## Tiers
Once you are promoted to Tier, you are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. **You have been warned.**

Tiers have access to the !call PBST command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at a training, where you will be given temporary admin powers which are **strictly** for that specific training. Abuse of these powers will result in severe punishment.

## Tier 4 (aka Special Defense, SD)
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “Passed SD Eval” until the Trainers choose to promote you to Tier 4. Having Discord is required.

As Tier 4, you receive the ability to place a KoS in Pinewood facilities, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used responsibly.

Tier 4’s can host trainings with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request a training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions. Do not ask Trainers to log your points or promote you, be patient.

Trainers who are patrolling Pinewood facilities are exempt from most rules in the handbook, though the following reminders also apply to them.

# Reminders to all PBST members
  * Please abide by the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) at all times.
  * Be respectful to your fellow players, don’t act like you’re the boss of the game.
  * Follow the orders of anyone who outranks you.

::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::
