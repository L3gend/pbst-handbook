﻿---
home: true
heroImage: /PBST-Logo.png
actionText: Let's do this →
actionLink: pbst/
---
::: warning NOTE:
The handbook was originally made by RogueVader1996. All credit for creating the rules we stand on are for him.
:::

::: danger WARNING!
Please read through all of the rules very carefully. Any changes to the rules will be made clear. The rules have been categorized into who they apply to, however some rules may apply to more than one group of PBST members, in which case it will be made clear. Punishments for breaking a rule depend on what rule and the severity. Punishments can range from a simple warning, demotion, or even blacklist from PBST. 
:::